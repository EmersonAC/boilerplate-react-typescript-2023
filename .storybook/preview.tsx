import React from 'react'
import { ThemeProvider } from 'styled-components'
import { GlobalStyles } from '../src/styles/GlobalStyles'
import { theme } from '../src/styles/theme'
import type { Preview } from '@storybook/react'

export const preview: Preview = {
	parameters: {
		actions: { argTypesRegex: '^on[A-Z].*' },
		controls: {
			matchers: {
				color: /(background|color)$/i,
				date: /Date$/,
			},
		},
	},
}

export const decorators = [
	(Story, context) => (
		<ThemeProvider theme={theme}>
			<GlobalStyles />
			<Story />
		</ThemeProvider>
	),
]
