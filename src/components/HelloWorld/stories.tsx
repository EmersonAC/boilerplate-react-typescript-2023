import type { Meta, StoryObj } from '@storybook/react'

import { HelloWorld } from './HelloWorld'

const meta = {
	title: 'Example/HelloWorld',
	component: HelloWorld,
	parameters: {
		layout: 'fullscreen',
	},
} satisfies Meta<typeof HelloWorld>

export default meta
type Story = StoryObj<typeof meta>

export const Primary: Story = {
	args: { title: 'Hello StoryBook...' },
	render: (args) => <HelloWorld {...args} />,
}
